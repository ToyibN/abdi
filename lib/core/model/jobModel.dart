class FullJob {
  String id;
  String name;
  String salary;
  String description;
  String ownerName;

  FullJob({this.id,this.name, this.salary, this.description, this.ownerName});


  FullJob.fromMap(Map<String, dynamic> snapshot, String id) : id = id ?? '', name = snapshot['name'] ?? '', salary = snapshot['salary'] ?? '', description = snapshot['description'] ?? '', ownerName = snapshot['ownerName'];
  toJson(){
    return {
      "name" : name,
      "salary" : salary,
      "description" : description,
      "ownerName" : ownerName,
    };
  } 
}

class PartJob {
  String id;
  String name;
  String salary;
  String description;
  String ownerName;

  PartJob({this.id,this.name, this.salary, this.description, this.ownerName});


  PartJob.fromMap(Map<String, dynamic> snapshot, String id) : id = id ?? '', name = snapshot['name'] ?? '', salary = snapshot['salary'] ?? '', description = snapshot['description'] ?? '', ownerName = snapshot['ownerName'];
  toJson(){
    return {
      "name" : name,
      "salary" : salary,
      "description" : description,
      "ownerName" : ownerName,
    };
  } 
}

