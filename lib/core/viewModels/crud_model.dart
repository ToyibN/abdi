import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:abdi/core/services/api.dart';
import 'package:abdi/core/model/jobModel.dart';
import 'package:abdi/locator.dart';


class CRUDModel extends ChangeNotifier{
  Api _api = locator<Api>();

  List<FullJob> fulljob;

    Future<List<FullJob>> fetchProducts() async {
    var result = await _api.getDataCollection();
    fulljob = result.documents.map((doc) => FullJob.fromMap(doc.data, doc.documentID)).toList();
    return fulljob;
  }

  Stream<QuerySnapshot> fetchProductsAsStream() {
    return _api.streamDataCollection();
  }

  Future<FullJob> getProductById(String id) async {
    var doc = await _api.getDocumentById(id);
    return FullJob.fromMap(doc.data, doc.documentID);
  }

  Future removeProduct(String id) async {
    await _api.removeDocument(id);
    return;
  }

  Future updateProduct(FullJob data, String id) async {
    await _api.updateDocument(data.toJson(), id);
    return ;
  }

  Future addProduct(FullJob data) async{
    var result = await _api.addDocument(data.toJson());
    return;
  }

}

class CRUDModelPart extends ChangeNotifier {
  Api _partApi = locator2<Api>();

  List<PartJob> partjob;

    Future<List<PartJob>> fetchProducts() async {
    var result = await _partApi.getDataCollection();
    partjob = result.documents.map((doc) => PartJob.fromMap(doc.data, doc.documentID)).toList();
    return partjob;
  }

  Stream<QuerySnapshot> fetchProductsAsStream() {
    return _partApi.streamDataCollection();
  }

  Future<PartJob> getProductById(String id) async {
    var doc = await _partApi.getDocumentById(id);
    return PartJob.fromMap(doc.data, doc.documentID);
  }

  Future removeProduct(String id) async {
    await _partApi.removeDocument(id);
    return;
  }

  Future updateProduct(PartJob data, String id) async {
    await _partApi.updateDocument(data.toJson(), id);
    return ;
  }

  Future addProduct(PartJob data) async{
    var result = await _partApi.addDocument(data.toJson());
    return;
  }
}