import 'package:flutter/material.dart';

class ColorPalette{
  static const dotColor = Color(0xffe8e8e8);
  static const dotActive = Color(0xffff3800);
  static const titleColor = Color(0xff1c1a1a);
  static const descriptionColor = Color(0xff707070);


  static const primaryColor = Color(0xff5647B2);
  static const secondaryColor = Color(0xffF9BC04);
  static const thirdColor = Color(0xffFF8056);
  static const primaryDarkColor = Color(0xff607cbf);
  static const underlineTextField = Color(0xff8b97ff);
  static const hintColor = Color(0xffccd1ff);

}