import 'package:abdi/core/viewModels/crud_model.dart';
import 'package:abdi/home_view.dart';
import 'package:abdi/locator.dart';
import 'package:abdi/screens/full_time.dart';
import 'package:abdi/screens/login_view.dart';
import 'package:abdi/screens/register_view.dart';
import 'package:flutter/material.dart';
import 'package:abdi/screens/intro_view.dart';
import 'package:provider/provider.dart';


void main() {
  setupLocator();
  runApp(MyApp());
  }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => locator<CRUDModel>(),),
        ChangeNotifierProvider(builder: (_) => locator2<CRUDModelPart>(),),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: IntroPage(),
        title: 'Intro Screen',
        routes: <String, WidgetBuilder>{
          '/LoginPage' : (BuildContext context) => LoginPage(),
          '/RegisterPage' : (BuildContext context) => RegisterPage(),
          '/Home' : (BuildContext context) => HomePage(title: 'Task',),
          '/IntroPage' : (BuildContext context) => IntroPage(),
          '/FullTime' : (BuildContext context) => FullTime(),
        },
      ),
    );
  }
}


