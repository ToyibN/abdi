import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:abdi/share/color_palette.dart';
import 'package:abdi/screens/home_content.dart' as home;
import 'package:abdi/screens/myjobs_content.dart' as jobs;
import 'package:abdi/screens/notifications_content.dart' as notifications;
import 'package:abdi/screens/profile.dart' as profile;
import 'dart:async';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title, this.uid,})
      : super(key: key);

  final String title;
  final String uid;

  @override
  _HomePageState createState() => _HomePageState();
  
}

class _HomePageState extends State<HomePage> {
  // final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  FirebaseUser currentUser;

  @override
  void initState() { 
    this.getCurrentUser();
    super.initState();
  }

  void getCurrentUser() async {
    currentUser = await FirebaseAuth.instance.currentUser();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          // key: _scaffoldKey,
          appBar: AppBar(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(widget.title),
                Image.asset(
                  'asset/icon/man.png',
                  fit: BoxFit.cover,
                  width: 50,
                  height: 50,
                ),
              ],
            ),
          backgroundColor: ColorPalette.primaryColor,
          // leading: IconButton(
          //   icon: Icon(Icons.dehaze),
          //   onPressed: () => _scaffoldKey.currentState.openDrawer(),
          // ),
        ),

      //   drawer: Drawer(
      //   child: ListView(
      //     padding: EdgeInsets.zero,
      //     children: <Widget>[
      //       DrawerHeader(
      //         child: Text(
      //           '',
      //           style: TextStyle(),
      //         ),
      //         decoration: BoxDecoration(
      //           color: Colors.blue,
      //         ),
      //       ),
      //       ListTile(
      //         title: Text('data'),
      //         onTap: null,
      //       ),
      //       ListTile(
      //         title: Text('data 2'),
      //         onTap: null,
      //       )
      //     ],
      //   ),
      // ),

        bottomNavigationBar: menu(),
        
        body: TabBarView(
          children: <Widget>[
            home.HomePage(),
            jobs.MyJobs(),
            notifications.Notifications(),
            profile.Profile(),
          ],
        ),
      
        ),
      ),
    );
  }
  }


// class HomePage extends StatelessWidget {
//   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: DefaultTabController(
//         length: 4,
//         child: Scaffold(
//           key: _scaffoldKey,
//           appBar: AppBar(
//             title: Row(
//               mainAxisAlignment: MainAxisAlignment.end,
//               children: <Widget>[
//                 Image.asset(
//                   'asset/icon/man.png',
//                   fit: BoxFit.cover,
//                   width: 50,
//                   height: 50,
//                 ),
//               ],
//             ),
//           backgroundColor: ColorPalette.primaryColor,
//           leading: IconButton(
//             icon: Icon(Icons.dehaze),
//             onPressed: () => _scaffoldKey.currentState.openDrawer(),
//           ),
//         ),

//         drawer: Drawer(
//         child: ListView(
//           padding: EdgeInsets.zero,
//           children: <Widget>[
//             DrawerHeader(
//               child: Text(
//                 '',
//                 style: TextStyle(),
//               ),
//               decoration: BoxDecoration(
//                 color: Colors.blue,
//               ),
//             ),
//             ListTile(
//               title: Text('data'),
//               onTap: null,
//             ),
//             ListTile(
//               title: Text('data 2'),
//               onTap: null,
//             )
//           ],
//         ),
//       ),

//         bottomNavigationBar: menu(),
        
//         body: TabBarView(
//           children: <Widget>[
//             home.HomePage(),
//             jobs.MyJobs(),
//             notifications.Notifications(),
//             profile.Profile(),
//           ],
//         ),
      
//         ),
//       ),
//     );
//   }

  Widget menu() {
    return Container(
      color: ColorPalette.primaryColor,
      // decoration: BoxDecoration(
      //   boxShadow: [
      //     BoxShadow(
      //       color: Colors.black,
      //       blurRadius: 2.0,
      //       spreadRadius: 10.0,
      //       offset: Offset(
      //         5.0, 5.0)
      //     )
      //   ]
      // ),
      child: TabBar(
        labelColor: ColorPalette.thirdColor,
        unselectedLabelColor: Colors.white70,
        indicatorSize: TabBarIndicatorSize.tab,
        indicatorPadding: EdgeInsets.all(5.0),
        indicatorColor: ColorPalette.thirdColor,
        tabs: <Widget>[
          Tab(
            child: Text('Home'),
            icon: Icon(Icons.home,),
          ),
          Tab(
            text: 'My Jobs',
            icon: Icon(Icons.work),
          ),
          Tab(
            text: 'Notifications',
            icon: Icon(Icons.notifications),
          ),
          Tab(
            text: 'Profile',
            icon: Icon(Icons.person),
          ),
        ],
      ),
    );
  }