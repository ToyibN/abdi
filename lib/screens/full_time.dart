import 'package:abdi/share/color_palette.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:abdi/core/viewModels/crud_model.dart';
import 'package:abdi/core/model/jobModel.dart';
import 'package:abdi/screens/widgets/jobCard.dart';
import 'package:provider/provider.dart';

class FullTime extends StatefulWidget {
  @override
  _FullTimeState createState() => _FullTimeState();
}

class _FullTimeState extends State<FullTime> {
  List<FullJob> fullJob;

  @override
  Widget build(BuildContext context) {
    final productProvider = Provider.of<CRUDModel>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Search Full Jobs'),
        backgroundColor: ColorPalette.primaryColor,
      ),
      body: Container(
        child: Flex(
          direction: Axis.vertical,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextFormField(
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: ColorPalette.primaryColor),
                        borderRadius: BorderRadius.circular(15.0)),
                    prefixIcon: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          child: Icon(Icons.search),
                          padding: EdgeInsets.only(right: 10.0),
                        )
                      ],
                    ),
                    hintText: 'Search',
                    hoverColor: ColorPalette.primaryColor,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0))),
                autofocus: false,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Card(
                elevation: 3,
                child: Container(
                  height: 25.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Recents'),
                      SizedBox(width: 50.0),
                      Text('Nearest'),
                      SizedBox(width: 50.0),
                      Text('Most Applied'),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: StreamBuilder(
                stream: productProvider.fetchProductsAsStream(),
                builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasData) {
                    fullJob = snapshot.data.documents
                        .map((doc) => FullJob.fromMap(doc.data, doc.documentID))
                        .toList();
                    return ListView.builder(
                      itemCount: fullJob.length,
                      itemBuilder: (buildContext, index) =>
                          JobCard(fullJobDetails: fullJob[index]),
                    );
                  } else {
                    return Scaffold(
                      body: Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
