// import 'package:abdi/authentication.dart';
import 'package:abdi/share/color_palette.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:abdi/screens/auth_google.dart';

import '../home_view.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();
  TextEditingController emailInputController;
  TextEditingController pwdInputController;
  String errorMessage;

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn _googleSignIn = GoogleSignIn();

  Future<FirebaseUser> _signIn() async {
    GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication authentication =
        await googleSignInAccount.authentication;
  }

  @override
  initState() {
    emailInputController = new TextEditingController();
    pwdInputController = new TextEditingController();
    super.initState();
  }

  String emailValidator(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value)) {
      return 'Email format is invalid';
    } else {
      return null;
    }
  }

  String pwdValidator(String value) {
    if (value.length < 8) {
      return 'Password must be longer than 8 characters';
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('asset/icon/background.png'),
                    fit: BoxFit.cover)),
            padding: EdgeInsets.only(left: 25.0, right: 25.0),
            child: SingleChildScrollView(
                child: Form(
              key: _loginFormKey,
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 35.0,
                  ),
                  Image.asset(
                    'asset/icon/Logo_2.png',
                    fit: BoxFit.contain,
                  ),
                  SizedBox(
                    height: 70.0,
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: ColorPalette.primaryColor),
                            borderRadius: BorderRadius.circular(20.0)),
                        prefixIcon: Icon(
                          Icons.email,
                          color: ColorPalette.primaryColor,
                        ),
                        hintText: 'Email',
                        hoverColor: ColorPalette.primaryColor,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0))),
                    autofocus: false,
                    controller: emailInputController,
                    keyboardType: TextInputType.emailAddress,
                    validator: emailValidator,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  TextFormField(
                    autofocus: false,
                    obscureText: true,
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: ColorPalette.primaryColor),
                            borderRadius: BorderRadius.circular(20.0)),
                        fillColor: ColorPalette.primaryColor,
                        prefixIcon: Icon(
                          Icons.lock,
                          color: ColorPalette.primaryColor,
                        ),
                        hintText: 'password',
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: ColorPalette.primaryColor),
                          borderRadius: BorderRadius.circular(32.0),
                        )),
                    controller: pwdInputController,
                    validator: pwdValidator,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.0),
                  ),
                  Material(
                    borderRadius: BorderRadius.circular(30.0),
                    shadowColor: ColorPalette.primaryColor,
                    elevation: 5.0,
                    color: ColorPalette.primaryColor,
                    child: MaterialButton(
                      minWidth: 300.0,
                      height: 42.0,
                      onPressed: () {
                        if (_loginFormKey.currentState.validate()) {
                          FirebaseAuth.instance
                              .signInWithEmailAndPassword(
                                  email: emailInputController.text,
                                  password: pwdInputController.text)
                              .then((currentUser) => Firestore.instance
                                  .collection("users")
                                  .document(currentUser.user.uid)
                                  .get()
                                  .then((DocumentSnapshot result) =>
                                      Navigator.pushReplacement(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => HomePage(
                                                    title: "" + result["fname"],
                                                    uid: currentUser.user.uid,
                                                  ))))
                                  .catchError((err) => print(err)))
                              .catchError((err) {
                            switch (err.code) {
                              case 'ERROR_WRONG_PASSWORD':
                                errorMessage = 'Wrong Password';
                                break;

                              case 'ERROR_INVALID_EMAIL':
                                errorMessage =
                                    'Your email address appears to be malformed.';
                                break;

                              case 'ERROR_USER_NOT_FOUND':
                                errorMessage =
                                    "User with this email doesn't exist.";
                                break;

                              case 'ERROR_USER_DISABLED':
                                errorMessage =
                                    'User with this email has been disabled.';
                                break;

                              case 'ERROR_TOO_MANY_REQUEST':
                                errorMessage =
                                    'Too Many Request, Try Again Later';
                                break;

                              case "ERROR_OPERATION_NOT_ALLOWED":
                                errorMessage =
                                    "Signing in with Email and Password is not enabled.";
                                break;

                              default:
                                print('An undefined Error happened');
                            }
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text('Login Error'),
                                    content: Text('$errorMessage'),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text('Close'),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      )
                                    ],
                                  );
                                });
                          });
                        }
                      },
                      child: Text(
                        "LOGIN",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Material(
                    borderRadius: BorderRadius.circular(30.0),
                    shadowColor: ColorPalette.primaryColor,
                    elevation: 5.0,
                    color: ColorPalette.primaryColor,
                    child: MaterialButton(
                      minWidth: 300.0,
                      height: 42.0,
                      onPressed: () {
                        Navigator.pushNamed(context, '/RegisterPage');
                      },
                      child: Text(
                        "SIGN UP",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  _signInGoogle(),
                  SizedBox(
                    height: 10.0,
                  ),
                  FlatButton(
                    child: Text(
                      "Forgot Password?",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    onPressed: null,
                  ),
                ],
              ),
            ))));
  }

  Widget _signInGoogle() {
    return OutlineButton(
      splashColor: Colors.grey,
      onPressed: () {
        signInWithGoogle().whenComplete(() {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return HomePage();
              },
            ),
          );
        });
      },
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      highlightElevation: 0,
      borderSide: BorderSide(color: Colors.grey),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image(
                image: AssetImage("asset/icon/google_logo.png"), height: 20.0),
            Padding(
              padding: const EdgeInsets.only(left: 10),
              child: Text(
                'Sign in with Google',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.grey,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

//ini sengaja buat dokumentasi

//   final GlobalKey<FormState> _loginFormKey = GlobalKey<FormState>();
//   TextEditingController emailInputController;
//   TextEditingController pwdInputController;

//   @override
//   void initState() {
//     emailInputController = TextEditingController();
//     pwdInputController = TextEditingController();
//     super.initState();
//   }

//   String emailValidator(String value) {
//     Pattern pattern =
//         r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
//     RegExp regex = new RegExp(pattern);
//     if (!regex.hasMatch(value)) {
//       return 'Email format is invalid';
//     } else {
//       return null;
//     }
//   }

//   String pwdValidator(String value) {
//     if (value.length < 8) {
//       return 'Password must be longer than 8 characters';
//     } else {
//       return null;
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: Stack(
//         children: <Widget>[
//           showForm(),
//         ],
//       ),
//     );
//   }

// Widget showForm() {
//   return Container(
//     padding: EdgeInsets.all(16.0),
//     child: SingleChildScrollView(
//         child: Form(
//         key: _loginFormKey,
//         child: ListView(
//             shrinkWrap: true,
//             padding: EdgeInsets.only(left: 24.0, right: 24.0),
//             children: <Widget>[
//               showLogo(),
//               showEmail(),
//               SizedBox(
//                 height: 25.0,
//               ),
//               showPassword(),
//               SizedBox(
//                 height: 20.0,
//               ),
//               showLogin(),
//               showSignUp(),
//               showLabel(),
//             ],
//           ),
//       ),
//     ),
//   );
// }

//   Widget showLogo() {
//     return Center(
//       child: Image.asset(
//         'asset/icon/Logo_2.png',
//         height: 250.0,
//         width: 250.0,
//         fit: BoxFit.contain,
//       ),
//     );
//   }

//   Widget showEmail() {
//     return Center(
//       child: TextFormField(
//         keyboardType: TextInputType.emailAddress,
//         autofocus: false,
//         decoration: InputDecoration(
//             enabledBorder: OutlineInputBorder(
//                 borderSide: BorderSide(color: ColorPalette.primaryColor),
//                 borderRadius: BorderRadius.circular(20.0)),
//             prefixIcon: Icon(
//               Icons.email,
//               color: ColorPalette.primaryColor,
//             ),
//             hintText: 'Email',
//             hoverColor: ColorPalette.primaryColor,
//             contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//             border:
//                 OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
//         validator: emailValidator,
//       ),
//     );
//   }

//   Widget showPassword() {
//     return Center(
//       child: TextFormField(
//         autofocus: false,
//         obscureText: true,
//         decoration: InputDecoration(
//             enabledBorder: OutlineInputBorder(
//                 borderSide: BorderSide(color: ColorPalette.primaryColor),
//                 borderRadius: BorderRadius.circular(20.0)),
//             fillColor: ColorPalette.primaryColor,
//             prefixIcon: Icon(
//               Icons.lock,
//               color: ColorPalette.primaryColor,
//             ),
//             hintText: 'password',
//             contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//             border: OutlineInputBorder(
//               borderSide: const BorderSide(color: ColorPalette.primaryColor),
//               borderRadius: BorderRadius.circular(32.0),
//             )),
//             validator: pwdValidator,
//       ),
//     );
//   }

//   Widget showLogin() {
//     return Padding(
//       padding: EdgeInsets.symmetric(vertical: 16.0),
//       child: Material(
//         borderRadius: BorderRadius.circular(30.0),
//         shadowColor: ColorPalette.primaryColor,
//         elevation: 5.0,
//         color: ColorPalette.primaryColor,
//         child: MaterialButton(
//           minWidth: 200.0,
//           height: 42.0,
//           onPressed: () {
//             if (_loginFormKey.currentState.validate()) {
//               FirebaseAuth.instance
//                   .signInWithEmailAndPassword(
//                       email: emailInputController.text,
//                       password: pwdInputController.text)
//                   .then((currentUser) => Firestore.instance
//                       .collection("users")
//                       .document(currentUser.user.uid)
//                       .get()
//                       .then((DocumentSnapshot result) =>
//                           Navigator.pushReplacement(
//                               context,
//                               MaterialPageRoute(
//                                   builder: (context) => HomePage(
//                                         title: result["fname"] +
//                                             "'s Tasks",
//                                         uid: currentUser.user.uid,
//                                       ))))
//                       .catchError((err) => print(err)))
//                   .catchError((err) => print(err));
//             }
//           },
//           child: Text(
//             "LOGIN",
//             style: TextStyle(color: Colors.white),
//           ),
//         ),
//       ),
//     );
//   }

//   Widget showSignUp() {
//     return Padding(
//       padding: EdgeInsets.symmetric(vertical: 16.0),
//       child: Material(
//         borderRadius: BorderRadius.circular(30.0),
//         shadowColor: ColorPalette.primaryColor,
//         elevation: 5.0,
//         color: ColorPalette.primaryColor,
//         child: MaterialButton(
//           minWidth: 200.0,
//           height: 42.0,
//           onPressed: () {
//             //Navigator.pushNamed mean that it will be move when you click and it alson not deleting the last page
//             Navigator.of(context).pushNamed('/RegisterPage');
//           },
//           child: Text(
//             'SIGN UP',
//             style: TextStyle(color: Colors.white),
//           ),
//         ),
//       ),
//     );
//   }

//   Widget showLabel() {
//     return Center(
//       child: FlatButton(
//         child: Text(
//           'Forgot Password ?',
//           style: TextStyle(color: Colors.black54),
//         ),
//         onPressed: () {},
//       ),
//     );
//   }
// }

// class LoginPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     final logo = Image.asset(
//       'asset/icon/Logo_2.png',
//       height: 250.0,
//       width: 250.0,
//       fit: BoxFit.contain,
//     );

//     final email = TextFormField(
//       keyboardType: TextInputType.emailAddress,
//       autofocus: false,
//       decoration: InputDecoration(
//         enabledBorder: OutlineInputBorder(
//           borderSide: BorderSide(color: ColorPalette.primaryColor),
//           borderRadius: BorderRadius.circular(20.0)
//         ),
//           prefixIcon: Icon(Icons.email, color: ColorPalette.primaryColor,),
//           hintText: 'Email',
//           hoverColor: ColorPalette.primaryColor,
//           contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//           border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
//     );

//     final password = TextFormField(
//       autofocus: false,
//       obscureText: true,
//       decoration: InputDecoration(
//         enabledBorder: OutlineInputBorder(
//           borderSide: BorderSide(color: ColorPalette.primaryColor),
//           borderRadius: BorderRadius.circular(20.0)
//         ),
//           fillColor: ColorPalette.primaryColor,
//           prefixIcon: Icon(Icons.lock, color: ColorPalette.primaryColor,),
//           hintText: 'password',
//           contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//           border: OutlineInputBorder(
//               borderSide: const BorderSide(color: ColorPalette.primaryColor),
//               borderRadius: BorderRadius.circular(32.0),
//               )
//               ),
//     );

//     final loginButton = Padding(
//       padding: EdgeInsets.symmetric(vertical: 16.0),
//       child: Material(
//         borderRadius: BorderRadius.circular(30.0),
//         shadowColor: ColorPalette.primaryColor,
//         elevation: 5.0,
//         color: ColorPalette.primaryColor,
//         child: MaterialButton(
//           minWidth: 200.0,
//           height: 42.0,
//           onPressed: () {
//             //Navigator.pushReplacement mean that if u already move it will be delete the last screen so you can't back again to last screen, is usually used in login or transaction button
//             Navigator.of(context)
//                 .pushReplacement(MaterialPageRoute(builder: (_) {
//               return HomePage();
//             }));
//           },
//           child: Text(
//             'LOG IN',
//             style: TextStyle(color: Colors.white),
//           ),
//         ),
//       ),
//     );

//     final signupButton = Padding(
//       padding: EdgeInsets.symmetric(vertical: 16.0),
//       child: Material(
//         borderRadius: BorderRadius.circular(30.0),
//         shadowColor: ColorPalette.primaryColor,
//         elevation: 5.0,
//         color: ColorPalette.primaryColor,
//         child: MaterialButton(
//           minWidth: 200.0,
//           height: 42.0,
//           onPressed: () {
//             //Navigator.pushNamed mean that it will be move when you click and it alson not deleting the last page
//             Navigator.of(context).pushNamed('/RegisterPage');
//           },
//           child: Text(
//             'SIGN UP',
//             style: TextStyle(color: Colors.white),
//           ),
//         ),
//       ),
//     );

//     final forgotLabel = FlatButton(
//       child: Text(
//         'Forgot Password ?',
//         style: TextStyle(color: Colors.black54),
//       ),
//       onPressed: () {},
//     );

//     return Scaffold(

//     );
//   }
// }
