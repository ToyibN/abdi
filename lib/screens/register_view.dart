import 'package:abdi/screens/after_register_view.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:abdi/home_view.dart';
import 'package:abdi/share/color_palette.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final GlobalKey<FormState> _registerFormKey = GlobalKey<FormState>();
  TextEditingController firstNameInputController;
  TextEditingController lastNameInputController;
  TextEditingController emailInputController;
  TextEditingController pwdInputController;
  TextEditingController confirmPwdInputController;

  @override
  initState() {
    firstNameInputController = new TextEditingController();
    lastNameInputController = new TextEditingController();
    emailInputController = new TextEditingController();
    pwdInputController = new TextEditingController();
    confirmPwdInputController = new TextEditingController();
    super.initState();
  }

  String emailValidator(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value)) {
      return 'Email format is invalid';
    } else {
      return null;
    }
  }

  String pwdValidator(String value) {
    if (value.length < 8) {
      return 'Password must be longer than 8 characters';
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('asset/icon/background.png'),
              fit: BoxFit.cover
            )
          ),
            padding: const EdgeInsets.only(left: 25.0, right: 25.0),
            child: SingleChildScrollView(
                child: Form(
              key: _registerFormKey,
              child: Column(
                children: <Widget>[
                  Image.asset(
                    'asset/icon/Logo_2.png',
                    fit: BoxFit.contain,
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  TextFormField(
                    autofocus: false,
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: ColorPalette.primaryColor),
                            borderRadius: BorderRadius.circular(20.0)),
                        prefixIcon: Icon(
                          Icons.person,
                          color: ColorPalette.primaryColor,
                        ),
                        hintText: 'First Name',
                        hoverColor: ColorPalette.primaryColor,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0))),
                    controller: firstNameInputController,
                    validator: (value) {
                      if (value.length < 3) {
                        return "Please enter a valid first name.";
                      }
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                      keyboardType: TextInputType.emailAddress,
                      autofocus: false,
                      decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: ColorPalette.primaryColor),
                              borderRadius: BorderRadius.circular(20.0)),
                          prefixIcon: Icon(
                            Icons.person,
                            color: ColorPalette.primaryColor,
                          ),
                          hintText: 'Last Name',
                          hoverColor: ColorPalette.primaryColor,
                          contentPadding:
                              EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(32.0))),
                      controller: lastNameInputController,
                      validator: (value) {
                        if (value.length < 3) {
                          return "Please enter a valid last name.";
                        }
                      }),
                  SizedBox(
                    height: 15.0,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    autofocus: false,
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: ColorPalette.primaryColor),
                            borderRadius: BorderRadius.circular(20.0)),
                        prefixIcon: Icon(
                          Icons.email,
                          color: ColorPalette.primaryColor,
                        ),
                        hintText: 'Email',
                        hoverColor: ColorPalette.primaryColor,
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(32.0))),
                    controller: emailInputController,
                    validator: emailValidator,
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  TextFormField(
                    autofocus: false,
                    obscureText: true,
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: ColorPalette.primaryColor),
                            borderRadius: BorderRadius.circular(20.0)),
                        fillColor: ColorPalette.primaryColor,
                        prefixIcon: Icon(
                          Icons.lock,
                          color: ColorPalette.primaryColor,
                        ),
                        hintText: 'Password',
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: ColorPalette.primaryColor),
                          borderRadius: BorderRadius.circular(32.0),
                        )),
                    controller: pwdInputController,
                    validator: pwdValidator,
                  ),
                  SizedBox(height: 15.0,),
                  TextFormField(
                    autofocus: false,
                    obscureText: true,
                    decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: ColorPalette.primaryColor),
                            borderRadius: BorderRadius.circular(20.0)),
                        fillColor: ColorPalette.primaryColor,
                        prefixIcon: Icon(
                          Icons.lock,
                          color: ColorPalette.primaryColor,
                        ),
                        hintText: 'Password',
                        contentPadding:
                            EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        border: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: ColorPalette.primaryColor),
                          borderRadius: BorderRadius.circular(32.0),
                        )),
                    controller: confirmPwdInputController,
                    validator: pwdValidator,
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Material(
                    borderRadius: BorderRadius.circular(30.0),
                    shadowColor: ColorPalette.primaryColor,
                    elevation: 5.0,
                    color: ColorPalette.primaryColor,
                    child: MaterialButton(
                      minWidth: 200.0,
                      height: 42.0,
                      onPressed: () {
                        if (_registerFormKey.currentState.validate()) {
                          if (pwdInputController.text ==
                              confirmPwdInputController.text) {
                            FirebaseAuth.instance
                                .createUserWithEmailAndPassword(
                                    email: emailInputController.text,
                                    password: pwdInputController.text)
                                .then((currentUser) => Firestore.instance
                                    .collection("users")
                                    .document(currentUser.user.uid)
                                    .setData({
                                      "uid": currentUser.user.uid,
                                      "fname": firstNameInputController.text,
                                      "surname": lastNameInputController.text,
                                      "email": emailInputController.text,
                                    })
                                    .then((result) => {
                                        Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => HomePage(
                                                      title:
                                                              "Welcome "  +
                                                              firstNameInputController.text,
                                                      uid: currentUser.user.uid,
                                                    )),
                                            (_) => false),
                                        firstNameInputController.clear(),
                                        lastNameInputController.clear(),
                                        emailInputController.clear(),
                                        pwdInputController.clear(),
                                        confirmPwdInputController.clear()
                                      })
                                    .catchError((err) => print(err)))
                                .catchError((err) => print(err));
                          } else {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Error"),
                                    content: Text("The passwords do not match"),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text("Close"),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      )
                                    ],
                                  );
                                });
                          }
                        }
                      },
                      child: Text(
                        'REGISTER NOW',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 15.0,
                  ),
                  Text("Already have an account?"),
                  FlatButton(
                    child: Text("Login here!"),
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, '/LoginPage');
                    },
                  )
                ],
              ),
            ))));
  }
}

// class RegisterPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     final logo = Image.asset(
//       'asset/icon/Logo_2.png',
//       height: 250.0,
//       width: 250.0,
//     );

//     final fullName = TextFormField(
//       keyboardType: TextInputType.emailAddress,
//       autofocus: false,
//       decoration: InputDecoration(
//         enabledBorder: OutlineInputBorder(
//           borderSide: BorderSide(color: ColorPalette.primaryColor),
//           borderRadius: BorderRadius.circular(20.0)
//         ),
//           prefixIcon: Icon(Icons.person, color:ColorPalette.primaryColor,),
//           hintText: 'Full Name',
//           hoverColor: ColorPalette.primaryColor,
//           contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//           border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
//     );

//     final email = TextFormField(
//       keyboardType: TextInputType.emailAddress,
//       autofocus: false,
//       decoration: InputDecoration(
//         enabledBorder: OutlineInputBorder(
//           borderSide: BorderSide(color: ColorPalette.primaryColor),
//           borderRadius: BorderRadius.circular(20.0)
//         ),
//           prefixIcon: Icon(Icons.email, color: ColorPalette.primaryColor,),
//           hintText: 'Email',
//           hoverColor: ColorPalette.primaryColor,
//           contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//           border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
//     );

//     final password = TextFormField(
//       autofocus: false,
//       obscureText: true,
//       decoration: InputDecoration(
//         enabledBorder: OutlineInputBorder(
//           borderSide: BorderSide(color: ColorPalette.primaryColor),
//           borderRadius: BorderRadius.circular(20.0)
//         ),
//           fillColor: ColorPalette.primaryColor,
//           prefixIcon: Icon(Icons.lock, color: ColorPalette.primaryColor,),
//           hintText: 'Password',
//           contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//           border: OutlineInputBorder(
//               borderSide: const BorderSide(color: ColorPalette.primaryColor),
//               borderRadius: BorderRadius.circular(32.0),
//               )
//               ),
//     );

//     final confirmPassword = TextFormField(
//       autofocus: false,
//       obscureText: true,
//       decoration: InputDecoration(
//         enabledBorder: OutlineInputBorder(
//           borderSide: BorderSide(color: ColorPalette.primaryColor),
//           borderRadius: BorderRadius.circular(20.0)
//         ),
//           fillColor: ColorPalette.primaryColor,
//           prefixIcon: Icon(Icons.lock, color: ColorPalette.primaryColor,),
//           hintText: 'Confirm Password',
//           contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
//           border: OutlineInputBorder(
//               borderSide: const BorderSide(color: ColorPalette.primaryColor),
//               borderRadius: BorderRadius.circular(32.0),
//               )
//               ),
//     );

//     final registerNow = Padding(
//       padding: EdgeInsets.symmetric(vertical: 16.0),
//       child: Material(
//         borderRadius: BorderRadius.circular(30.0),
//         shadowColor: ColorPalette.primaryColor,
//         elevation: 5.0,
//         color: ColorPalette.primaryColor,
//         child: MaterialButton(
//           minWidth: 200.0,
//           height: 42.0,
//           onPressed: () {
//             //Navigator.pushReplacement mean that if u already move it will be delete the last screen so you can't back again to last screen, is usually used in login or transaction button
//             Navigator.of(context)
//                 .pushReplacement(MaterialPageRoute(builder: (_) {
//               return AfterRegister();
//             }));
//           },
//           child: Text(
//             'REGISTER NOW',
//             style: TextStyle(color: Colors.white),
//           ),
//         ),
//       ),
//     );

//     final alreadyHave = FlatButton(
//       child: Text('Already Have Account ? Sign In', style: TextStyle(color: ColorPalette.primaryColor),
//       ),
//       onPressed: () {
//         Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
//           return LoginPage();
//         }));
//       },
//     );

//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: Center(
//         child: ListView(
//           shrinkWrap: true,
//           padding: EdgeInsets.only(left: 24.0, right: 24.0),
//           children: <Widget>[
//             logo,
//             fullName,
//             SizedBox(
//               height: 10.0,
//             ),
//             email,
//             SizedBox(
//               height: 10.0,
//             ),
//             password,
//             SizedBox(
//               height: 10.0,
//             ),
//             confirmPassword,
//             SizedBox(
//               height: 10.0,
//             ),
//             registerNow,
//             alreadyHave
//           ],
//         ),
//       ),
//     );
//   }
// }
