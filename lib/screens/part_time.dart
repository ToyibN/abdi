import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:abdi/core/model/jobModel.dart';
import 'package:abdi/core/viewModels/crud_model.dart';
import 'package:abdi/share/color_palette.dart';
import 'package:abdi/screens/widgets/partCard.dart';

class PartTime extends StatefulWidget {
  @override
  _PartTimeState createState() => _PartTimeState();
}

class _PartTimeState extends State<PartTime> {
  List<PartJob> partJob;

  @override
  Widget build(BuildContext context) {
    final productProvider = Provider.of<CRUDModelPart>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Search Part Time Jobs'),
        backgroundColor: ColorPalette.primaryColor,
      ),
      body: Container(
        child: Flex(
          direction: Axis.vertical,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: TextFormField(
                decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                        borderSide:
                            BorderSide(color: ColorPalette.primaryColor),
                        borderRadius: BorderRadius.circular(15.0)),
                    prefixIcon: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          child: Icon(Icons.search),
                          padding: EdgeInsets.only(right: 10.0),
                        )
                      ],
                    ),
                    hintText: 'Search',
                    hoverColor: ColorPalette.primaryColor,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15.0))),
                autofocus: false,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Card(
                elevation: 3,
                child: Container(
                  height: 25.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Recents'),
                      SizedBox(width: 50.0),
                      Text('Nearest'),
                      SizedBox(width: 50.0),
                      Text('Most Applied'),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: StreamBuilder(
                stream: productProvider.fetchProductsAsStream(),
                builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasData) {
                    partJob = snapshot.data.documents
                        .map((doc) => PartJob.fromMap(doc.data, doc.documentID))
                        .toList();
                    return ListView.builder(
                      itemCount: partJob.length,
                      itemBuilder: (buildContext, index) =>
                          PartCard(partJobDetails: partJob[index]),
                    );
                  } else {
                    return Scaffold(
                      body: Container(
                        child: Center(
                          child: CircularProgressIndicator(),
                        ),
                      ),
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
