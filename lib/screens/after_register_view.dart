import 'package:abdi/home_view.dart';
import 'package:abdi/share/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:gender_selector/gender_selector.dart';

class AfterRegister extends StatefulWidget {
  @override
  _AfterRegisterState createState() => _AfterRegisterState();
}

class _AfterRegisterState extends State<AfterRegister> {

  String _date = 'Not Set';
  String status = '';

  void _pilihStatus (String value){
    setState(() {
     status = value; 
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Padding(
          padding: EdgeInsets.all(
            MediaQuery.of(context).size.width/50
          ),
          child: Column(
            children: <Widget>[
              Padding( 
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.width/10
                ),
                child: Row(
                  children: <Widget>[
                    Text('Date of Birth', style: TextStyle(color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold),)
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(MediaQuery.of(context).size.width/50),
              ),

              // DATE TIME PICKER *still need repair bcs the text is in the middle

              RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30.0)
                ),
                elevation: 4.0,
                onPressed: (){
                  DatePicker.showDatePicker(
                    context,
                    showTitleActions: true,
                    minTime: DateTime(1950),
                    maxTime: DateTime(2019),
                    onConfirm: (date){
                      print('confirm $date');
                      _date = '${date.year} - ${date.month} - ${date.day}';
                      setState(() {
                        
                      });
                    },
                    currentTime: DateTime.now(), locale: LocaleType.id
                    );
                },
                child: Container(
                  alignment: Alignment.center,
                  height: 50.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.date_range, size: 20.0, color: Colors.white,),
                                Text('  $_date', style: TextStyle(color: Colors.white, fontSize: 15.0),)
                              ],
                            ),
                          )
                        ],
                      ),
                      Text(
                        ' Change',
                        style: TextStyle(color: Colors.white, fontSize: 15.0),
                      )
                    ],
                  ),
                ),
                color: ColorPalette.primaryColor,
              ),
              
              //end of DATE Picker

              //Gender Picker diambil dari library di pub.dev
            Padding(
              padding: EdgeInsets.only(top:10.0),
              child: Row(
                children: <Widget>[
                  Text('Gender', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20.0),)
                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height/100),
              child: GenderSelector(
                onChanged: (gender) async {
                  print(gender);
                },
              ),
            ),
            
            //Flutter Radio
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height/50),
              child: Row(
                children: <Widget>[
                  Text('Please Select One', style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),)
                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.width/100),
              child: Column(
                children: <Widget>[
                  RadioListTile(
                    value: '0',
                    title: Text("I'm Job Seeker"),
                    groupValue: status,
                    activeColor: ColorPalette.primaryColor,
                    subtitle: Text('Choose One'),
                    onChanged: (String value){
                      _pilihStatus(value);
                    },
                  ),

                  RadioListTile(
                    value: '1',
                    title: Text("I'm Job Seeker"),
                    groupValue: status,
                    activeColor: ColorPalette.primaryColor,
                    subtitle: Text('Choose One'),
                    onChanged: (String value){
                      _pilihStatus(value);
                    }
                  ),
                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.width/100),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton(
                    color: ColorPalette.primaryColor,
                    padding: EdgeInsets.only(top: 15.0, bottom: 15.0, right: 35.0, left: 35.0),
                    child: Text('   NEXT   ', style: TextStyle(color: Colors.white, fontSize: 17.0),),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                      side: BorderSide(color: ColorPalette.primaryColor)
                    ),
                    onPressed: (){
                      //Navigator.pushReplacement mean that if u already move it will be delete the last screen so you can't back again to last screen, is usually used in login or transaction button
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (_){
                          return HomePage();
                        })
                      );
                    },
                  ),
                ],
              ),
            )
            ],
            
            
          
            
          ),
        ),
      ),
    );
  }
}