import 'package:flutter/material.dart';
import 'package:abdi/core/model/jobModel.dart';
import 'package:abdi/screens/part_timeDetails.dart';

class PartCard extends StatelessWidget {
  final PartJob partJobDetails;

  PartCard({@required this.partJobDetails});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (_) => PartJobsDetails(partJob : partJobDetails)));

      },
      child: Padding(
        padding: EdgeInsets.all(8),
        child: Card(
          elevation: 5,
          child: Container(
            child: Column(
              children: <Widget>[
                SizedBox(height: 10,),
                Hero(
                  tag: partJobDetails.id,
                  child: Image.asset(
                    'asset/icon/man.png',
                    height: 30.0, width: 30.0,
                  ),
                ),
                SizedBox(height: 10.0,),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment:
                    MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Text(
                            partJobDetails.name,
                            style: TextStyle(
                                fontSize: 14,
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.bold
                                ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Text(
                            'Job Provider ${partJobDetails.ownerName}',
                            style: TextStyle(
                              fontSize: 11.0
                            ),
                          )
                        ],
                      ),
                      Text(
                        'Rp. ${partJobDetails.salary}',
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 15,
                            fontStyle: FontStyle.italic,
                            color: Colors.orangeAccent),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}