import 'package:flutter/material.dart';
import 'package:abdi/core/model/jobModel.dart';
import 'package:abdi/screens/full_time.dart';
import 'package:abdi/screens/full_timeDetails.dart';

class JobCard extends StatelessWidget {
  final FullJob fullJobDetails;

  JobCard({@required this.fullJobDetails});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (_) => FullJobsDetails(fullJob : fullJobDetails)));

      },
      child: Padding(
        padding: EdgeInsets.all(8),
        child: Card(
          elevation: 5,
          child: Container(
            // height: 200,
            // width: 200,
            child: Column(
              children: <Widget>[
                SizedBox(height: 10,),
                Hero(
                  tag: fullJobDetails.id,
                  child: Image.asset(
                    'asset/icon/man.png',
                    height: 30.0, width: 30.0,
                  ),
                ),
                SizedBox(height: 10.0,),
                Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment:
                    MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Text(
                            fullJobDetails.name,
                            style: TextStyle(
                                fontSize: 14,
                                decoration: TextDecoration.underline,
                                fontWeight: FontWeight.bold
                                ),
                          ),
                          SizedBox(
                            height: 5.0,
                          ),
                          Text(
                            'Job Provider ${fullJobDetails.ownerName}',
                            style: TextStyle(
                              fontSize: 11.0
                            ),
                          )
                        ],
                      ),
                      Text(
                        'Rp. ${fullJobDetails.salary}',
                        style: TextStyle(
                            fontWeight: FontWeight.w900,
                            fontSize: 15,
                            fontStyle: FontStyle.italic,
                            color: Colors.orangeAccent),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}