import 'dart:math';

import 'package:abdi/share/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:abdi/core/model/jobModel.dart';
import 'package:abdi/core/viewModels/crud_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';

class AddJobs extends StatefulWidget {
  @override
  _AddJobsState createState() => _AddJobsState();
}

class _AddJobsState extends State<AddJobs> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController ownerName;
  TextEditingController name;
  TextEditingController salary;
  TextEditingController description;

  final databaseReference = Firestore.instance;

  @override
  void initState() { 
    ownerName = TextEditingController();
    name = TextEditingController();
    salary = TextEditingController();
    description = TextEditingController();
    super.initState();
  }
 
  @override
  Widget build(BuildContext context) {
    var jobProvider = Provider.of<CRUDModel>(context);

    return Scaffold(
        appBar: AppBar(
        title: Text('Search Full Jobs'),
        backgroundColor: ColorPalette.primaryColor,
      ),
      body: SingleChildScrollView(
              child: Container(
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 20.0,
                ),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: ColorPalette.primaryColor),
                          borderRadius: BorderRadius.circular(20.0)),
                      prefixIcon: Icon(
                        Icons.person,
                        color: ColorPalette.primaryColor,
                      ),
                      hintText: 'Job Provider Name',
                      hoverColor: ColorPalette.primaryColor,
                      contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0))),
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Please enter a valid name.";
                    }
                  },
                  controller:ownerName,
                ),
                SizedBox(
                  height: 20.0,
                ),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: ColorPalette.primaryColor),
                          borderRadius: BorderRadius.circular(20.0)),
                      prefixIcon: Icon(
                        Icons.credit_card,
                        color: ColorPalette.primaryColor,
                      ),
                      hintText: 'Title',
                      hoverColor: ColorPalette.primaryColor,
                      contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0))),
                  validator: (value) {
                    if (value.isEmpty) {
                      return "Please enter a valid title.";
                    }
                  },
                  controller: name,
                ),
                SizedBox(
                  height: 20.0,
                ),
                TextFormField(
                  keyboardType: TextInputType.numberWithOptions(),
                  autofocus: false,
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: ColorPalette.primaryColor),
                          borderRadius: BorderRadius.circular(20.0)),
                      prefixIcon: Icon(
                        Icons.monetization_on,
                        color: ColorPalette.primaryColor,
                      ),
                      hintText: 'Salary',
                      hoverColor: ColorPalette.primaryColor,
                      contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0))),
                  validator: (value) {
                    if (value.length < 3) {
                      return "Please enter a valid salary.";
                    }
                  },
                  controller: salary,
                ),
                SizedBox(
                  height: 20.0,
                ),
                TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              BorderSide(color: ColorPalette.primaryColor),
                          borderRadius: BorderRadius.circular(20.0)),
                      prefixIcon: Icon(
                        Icons.credit_card,
                        color: ColorPalette.primaryColor,
                      ),
                      hintText: 'Description',
                      hoverColor: ColorPalette.primaryColor,
                      contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(32.0))),
                  validator: (value) {
                    if (value.length < 3) {
                      return "Please enter a valid description.";
                    }
                  },
                  controller: description ,
                ),
                SizedBox(
                  height: 20,
                ),
                Material(
                      borderRadius: BorderRadius.circular(30.0),
                      shadowColor: ColorPalette.primaryColor,
                      elevation: 5.0,
                      color: ColorPalette.primaryColor,
                      child: MaterialButton(
                        minWidth: 300.0,
                        height: 42.0,
                        onPressed: () async {
                          await databaseReference.collection('fullJobs').add({ "ownerName" : ownerName.text, "name" : name.text, "salary" : salary.text, "description" : description.text}).then((result) => {
                            Navigator.pop(context),
                            ownerName.clear(),
                            name.clear(),
                            salary.clear(),
                            description.clear()
                          }).catchError((err) => print(err));


                          // if (_formKey.currentState.validate()) {
                          //   _formKey.currentState.save();
                          //   await jobProvider.addProduct(FullJob(name: name.text, salary: salary.text, description: description.text, ownerName: ownerName.text));
                          //   Navigator.pop(context);
                          // } else {
                          //   print('gamasuk');
                          // }
                        },
                        child: Text(
                          "Add Jobs",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}