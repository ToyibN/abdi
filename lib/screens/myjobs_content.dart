import 'package:abdi/screens/addJobs.dart';
import 'package:abdi/share/color_palette.dart';
import 'package:flutter/material.dart';


class MyJobs extends StatefulWidget {
  @override
  _MyJobsState createState() => _MyJobsState();
}

class _MyJobsState extends State<MyJobs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  height: 70,
                  width: MediaQuery.of(context).size.width,
                  child: Card(
                    color: Colors.white,
                    elevation: 5,
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(width: 10.0,),
                            Text('Ongoing', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20.0),),
                            SizedBox(width: 90.0,),
                            Text('|', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20.0),),
                            SizedBox(width: 90.0,),
                            Text('History', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20.0),),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30.0,
            ),
            Material(
              borderRadius: BorderRadius.circular(30.0),
              shadowColor: ColorPalette.primaryColor,
              elevation: 5.0,
              color: ColorPalette.primaryColor,
              child: MaterialButton(
                minWidth: 200.0,
                height: 42.0,
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (_)=> AddJobs()
                  ));
                },
                child: Text(
                  "Add Jobs",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
            // SizedBox(width: 50.0,),
            // Container(
            //   child: Card(
            //       color: ColorPalette.secondaryColor,
            //       elevation: 5,
            //       child: Container(
            //         child: Padding(
            //           padding: const EdgeInsets.all(8.0),
            //           child: Row(
            //             children: <Widget>[
            //               Icon(Icons.alarm),
            //               SizedBox(width: 10.0,),
            //               Text('Ongoing', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),)
            //             ],
            //           ),
            //         ),
            //       ),
            //     ),
            // ),
          ],
        ),
      ),
    );
  }
}