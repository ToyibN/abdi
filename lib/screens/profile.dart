import 'package:abdi/screens/auth_google.dart';
import 'package:abdi/screens/login_view.dart';
import 'package:abdi/share/color_palette.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Profile extends StatefulWidget {
  Profile({
    Key key,
    this.title,
    this.uid,
  }) : super(key: key);

  final String title;
  final String uid;

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  FirebaseUser currentUser;
  FirebaseAuth _auth = FirebaseAuth.instance;
  Firestore _firestore = Firestore.instance;

  @override
  void initState() {
    super.initState();
    _getCurrentUser();
  }

  Future <LoginPage> _signOut() async {
    await FirebaseAuth.instance.signOut();
    runApp(
      MaterialApp(
        home: LoginPage(),
        debugShowCheckedModeBanner: false,
      )
    );
  }

  void _getCurrentUser() async {
    currentUser = await _auth.currentUser();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: new Container(
        child: new Center(
            child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset(
                'asset/icon/man.png',
                height: 100,
                width: 100,
              ),
            ),
            Text(
              'Toyib Nurseha',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
            SizedBox(
              height: 10.0,
            ),
            Card(
              elevation: 5,
              child: Container(
                height: 35,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(Icons.card_membership),
                    ),
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          'First Name :',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )),
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          'currentUser.fname',
                          style: TextStyle(),
                        )),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Card(
              elevation: 5,
              child: Container(
                height: 35,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(Icons.card_membership),
                    ),
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          'Last Name :',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )),
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          'currentUser.surname',
                          style: TextStyle(),
                        )),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Card(
              elevation: 5,
              child: Container(
                height: 35,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(Icons.email),
                    ),
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          'Email :',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        )),
                    Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          'currentUser.email',
                          style: TextStyle(),
                        )),
        
                 
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            Material(
                    borderRadius: BorderRadius.circular(30.0),
                    shadowColor: ColorPalette.primaryColor,
                    elevation: 5.0,
                    color: ColorPalette.primaryColor,
                    child: MaterialButton(
                      minWidth: 200.0,
                      height: 42.0,
                      onPressed: _signOut,
                      child: Text(
                        "Log Out",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
          ],
        )),
      ),
    );
  }
}
