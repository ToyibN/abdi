import 'package:abdi/share/color_palette.dart';
import 'package:flutter/material.dart';

class Notifications extends StatefulWidget {
  @override
  _NotificationsState createState() => _NotificationsState();
}

class _NotificationsState extends State<Notifications> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Padding(
        padding: EdgeInsets.all(8),
        child: Column(
          children: <Widget>[
            Container(
              height: 70,
              child: Card(
                color: ColorPalette.secondaryColor,
                elevation: 5,
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.alarm),
                        SizedBox(width: 10.0,),
                        Text('Viola Telah menentukan pekerjanya', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),)
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: 70,
              child: Card(
                color: ColorPalette.secondaryColor,
                elevation: 5,
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.alarm),
                        SizedBox(width: 10.0,),
                        Text('Viola Telah menentukan pekerjanya', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),)
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: 70,
              child: Card(
                color: ColorPalette.secondaryColor,
                elevation: 5,
                child: Container(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Icon(Icons.alarm),
                        SizedBox(width: 10.0,),
                        Text('Viola Telah menentukan pekerjanya', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),)
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      ),
    );
  }
}