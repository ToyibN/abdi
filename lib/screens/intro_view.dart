import 'package:abdi/screens/login_view.dart';
import 'package:abdi/screens/register_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:abdi/home_view.dart';
import 'package:abdi/core/model/intro.dart';
import 'package:abdi/share/color_palette.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:abdi/main.dart';


class IntroPage extends StatefulWidget {
  IntroPage({Key key}) : super(key:key);

  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  // @override
  // initState() {
  //   FirebaseAuth.instance
  //       .currentUser()
  //       .then((currentUser) => {
  //             if (currentUser == null){
  //                Navigator.of(context).pushReplacement(
  //                     MaterialPageRoute(builder: (_){
  //                       return LoginPage();
  //             }))
  //             }
                
  //             else
  //               {
  //                 Firestore.instance
  //                     .collection("users")
  //                     .document(currentUser.uid)
  //                     .get()
  //                     .then((DocumentSnapshot result) =>
  //                         Navigator.pushReplacement(
  //                             context,
  //                             MaterialPageRoute(
  //                                 builder: (context) => HomePage(
  //                                     ))))
  //                     .catchError((err) => print(err))
  //               }
  //           })
  //       .catchError((err) => print(err));
  //   super.initState();
  // }

  final List <Intro> introList = [
    Intro(
      image: 'asset/icon/splash_screen1.png',
      title: 'For Clean Up Your House',
      description : 'Lorem ipsum is simply dummy text of the printing and typesetting industry'
    ),
    Intro(
      image: 'asset/icon/splash_screen2.png',
      title: 'Cook For You',
      description : 'Lorem ipsum is simply dummy text of the printing and typesetting industry'
    ),
    Intro(
      image: 'asset/icon/splash_screen3.png',
      title: 'Take Care of Your Children',
      description : 'Lorem ipsum is simply dummy text of the printing and typesetting industry'
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Swiper.children(
        index: 0,
        autoplay: false,
        loop: false,
        pagination: SwiperPagination(
          margin: EdgeInsets.only(bottom: 20.0),
          builder: DotSwiperPaginationBuilder(
            color: ColorPalette.dotColor,
            activeColor: ColorPalette.primaryColor,
            size: 10.0,
            activeSize: 10.0
          )
        ),
        control: SwiperControl(
          iconNext: null,
          iconPrevious: null,
        ),
        children: _buildPage(context),
      ),
    );
  }

  List <Widget> _buildPage(BuildContext context){
    List <Widget> widgets = [];

    for (int i = 0; i < introList.length; i++) {
      Intro intro = introList[i];
      widgets.add(
      Container(
        padding: EdgeInsets.only(top: 10.0),
        child: ListView(
          children: <Widget>[
            Image.asset(
              intro.image,
              width: 100.0,
              height: 350.0,
            ),
            Center(
              child: Text(
                intro.title,
                style: TextStyle(
                  color: ColorPalette.titleColor,
                  fontSize: 24.0,
                  fontWeight: FontWeight.bold
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                top: 10.0
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.height/20
              ),
              child: Text(
                intro.description,
                style: TextStyle(
                  color: ColorPalette.descriptionColor,
                  fontSize: 14.0
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: EdgeInsets.all(
                MediaQuery.of(context).size.height/30.0
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  color: ColorPalette.primaryColor,
                  padding: EdgeInsets.only(top: 15.0, bottom: 15.0, right: 35.0, left: 35.0),
                  child: Text('SIGN IN', style: TextStyle(color: Colors.white, fontSize: 17.0),),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: ColorPalette.primaryColor)
                  ),
                  onPressed: (){
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (_){
                        return LoginPage();
                      })
                    );
                  },
                ),
                Padding(
                  padding: EdgeInsets.all(
                    MediaQuery.of(context).size.height/20.0
                  ),
                ),
                RaisedButton(
                  color: ColorPalette.primaryColor,
                  padding: EdgeInsets.only(top: 15.0, bottom: 15.0, right: 35.0, left: 35.0),
                  child: Text('SIGN UP', style: TextStyle(color: Colors.white, fontSize: 17.0),),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                    side: BorderSide(color: ColorPalette.primaryColor)
                  ),
                  onPressed: (){
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (_){
                        return RegisterPage();
                      })
                    );
                  },
                ),
              ],
            ),
          ],
        ),
      )
    );
    }

    return widgets;
  }
}