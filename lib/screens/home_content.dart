import 'package:abdi/screens/full_time.dart';
import 'package:abdi/screens/part_time.dart';
import 'package:flutter/material.dart';
import 'package:abdi/share/color_palette.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:abdi/main.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(top: 30.0, left: 20.0),
                    child: Text(
                      'Select Job Type',
                      style:
                          TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold),
                    )),
              ],
            ),

            //card

            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.width/100),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 12.0),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (_) => FullTime()));
                    },
                      child: Card(
                      color: ColorPalette.thirdColor,
                      elevation: 5,
                      semanticContainer: true,
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                'Full Time',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0),
                              ),
                              Padding(padding: EdgeInsets.only(bottom: 30.0))
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.all(MediaQuery.of(context).size.width/100),
                            child: Image.asset(
                            'asset/icon/time-left.png',
                            width: 150.0,
                            height: 150.0,
                            fit: BoxFit.fill,
                          ),
                          ),
                          
                        ],
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      margin: EdgeInsets.all(10),
                    ),
                  ),

                  //Card ke 2

                  InkWell(
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (_) => PartTime()));
                    },
                      child: Card(
                      color: ColorPalette.secondaryColor,
                      elevation: 5,
                      semanticContainer: true,
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Text(
                                'Part Time',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0),
                              ),
                              Padding(padding: EdgeInsets.only(bottom: 30.0))
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.all(MediaQuery.of(context).size.width/100),
                            child : Image.asset(
                            'asset/icon/stopwatch.png',
                            width: 150.0,
                            height: 150.0,
                            fit: BoxFit.fill,
                          ),
                          ),
                          
                        ],
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      margin: EdgeInsets.all(10),
                    ),
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top:MediaQuery.of(context).size.width/50, left: 20),
                  child: Text(
                    'News & Article',
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height/40),
            ),
            //card samping
            Container(
              child: CarouselSlider(
                height: MediaQuery.of(context).size.height/5,
                autoPlay: true,
                initialPage: 0,
                enableInfiniteScroll: true,
                enlargeCenterPage: true,
                pauseAutoPlayOnTouch: Duration(seconds: 3),
                items: [
                  "asset/icon/cleaning.png", 
                  "asset/icon/cleaning2.png", 
                  "asset/icon/cleaning.png", 
                ].map((i){
                 return Builder(
                   builder: (BuildContext context){
                     return Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        image: DecorationImage(
                          image: AssetImage(i)
                        )
                      ),
                      // child: Text(''),
                     );
                   } 
                 ); 
                }).toList() ,
              ),
            )
          ],
        ),
      ),
    );
  }
}
