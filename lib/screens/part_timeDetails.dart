import 'package:abdi/core/viewModels/crud_model.dart';
import 'package:abdi/share/color_palette.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:abdi/core/model/jobModel.dart';

class PartJobsDetails extends StatelessWidget {
  final PartJob partJob;

  PartJobsDetails({@required this.partJob});

  @override
  Widget build(BuildContext context) {
      final productProvider = Provider.of<CRUDModelPart>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Job Details'),
        backgroundColor: ColorPalette.primaryColor,
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              SizedBox(height: 10,),
              Card(
                elevation: 5,
                child: Container(
                  height: 70,
                  child: Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Hero(
                          tag: partJob.id,
                          child: Image.asset(
                            'asset/icon/man.png',
                            height: 55,
                            width: 55,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 30.0),
                        child: Text(partJob.ownerName, style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10.0,),
              Card(
                elevation: 5,
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text('Title :', style: TextStyle(fontWeight: FontWeight.bold),)
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(partJob.name, style: TextStyle(),)
                          ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Card(
                elevation: 5,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text('Description :', style: TextStyle(fontWeight: FontWeight.bold),)
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(partJob.description, style: TextStyle(),)
                          ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Card(
                elevation: 5,
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text('Salary :', style: TextStyle(fontWeight: FontWeight.bold),)
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(partJob.salary, style: TextStyle(),)
                          ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10.0,),
              Card(
                elevation: 5,
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text('Duration :', style: TextStyle(fontWeight: FontWeight.bold),)
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(partJob.salary, style: TextStyle(),)
                          ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10.0,),
              Card(
                elevation: 5,
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text('Workplace :', style: TextStyle(fontWeight: FontWeight.bold),)
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Text(partJob.salary, style: TextStyle(),)
                          ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 15.0,),
              Material(
                      borderRadius: BorderRadius.circular(30.0),
                      shadowColor: ColorPalette.primaryColor,
                      elevation: 5.0,
                      color: ColorPalette.primaryColor,
                      child: MaterialButton(
                        minWidth: 200.0,
                        height: 42.0,
                        onPressed: null,
                        child: Text(
                          "Apply",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}