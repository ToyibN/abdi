import 'package:get_it/get_it.dart';
import 'package:abdi/core/services/api.dart';
import 'package:abdi/core/viewModels/crud_model.dart';
import 'package:provider/provider.dart';


GetIt locator = GetIt();
GetIt locator2 = GetIt();

void setupLocator() {
  locator.registerLazySingleton(()=> Api('fullJobs'));
  locator.registerLazySingleton(()=> CRUDModel());
  locator2.registerLazySingleton(() => Api('partJobs'));
  locator2.registerLazySingleton(()=> CRUDModelPart());
}